import "normalize.css";
import { AppProps } from "next/app";
// import { RestfulProvider } from "restful-react";

// NOTE: Do not move the styles dir to the src.
// They are used by the Netlify CMS preview feature.
import "../../public/styles/global.css";

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

// export default ({...props}: AppProps) =>
//   <RestfulProvider base="http://localhost:3001/api">
//     <App {...props}/>
//   </RestfulProvider>;