import { GetStaticPaths, GetStaticProps } from "next";
import Layout from "../../components/Layout";
import BasicMeta from "../../components/meta/BasicMeta";
import OpenGraphMeta from "../../components/meta/OpenGraphMeta";
import TwitterCardMeta from "../../components/meta/TwitterCardMeta";
import CategoryPostList from "../../components/CategoryPostList";
import config from "../../lib/config";
import { countCatogriesPosts, listCatogriesPostContent } from "../../lib/posts";
import { getCategory, listCategories } from "../../lib/categories";
import Head from "next/head";

export default function Index({ posts, category, pagination, page }) {
  const url = `/categories/${category.name}` + (page ? `/${page}` : "");
  const title = category.name;
  return (
    <Layout>
      <BasicMeta url={url} title={title} />
      <OpenGraphMeta url={url} title={title} />
      <TwitterCardMeta url={url} title={title} />
      <CategoryPostList posts={posts} category={category} pagination={pagination} />
    </Layout>
  );
}

export const getStaticProps = async ({ params }) => {
  const queries = params.slug;
  const [slug, page] = [queries[0], queries[1]];
  const posts = listCatogriesPostContent(
    page ? parseInt(page) : 1,
    config.posts_per_page,
    slug
  );
  const category = getCategory(slug);
  const pagination = {
    current: page ? parseInt(page) : 1,
    pages: Math.ceil(countCatogriesPosts(slug) / config.posts_per_page),
  };
  return {
    props: {
      posts,
      category,
      pagination,
    }
  };
};

export const getStaticPaths = async () => {
  const paths = listCategories().flatMap((category) => {
    const pages = Math.ceil(countCatogriesPosts(category.slug) / config.posts_per_page);
    return Array.from(Array(pages).keys()).map((page) =>
      page === 0
        ? {
            params: { slug: [category.slug] },
          }
        : {
            params: { slug: [category.slug, (page + 1).toString()] },
          }
    );
  });
  return {
    paths: paths,
    fallback: false,
  };
};
