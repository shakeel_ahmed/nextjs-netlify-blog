import Head from "next/head";
import Navigation from "./Navigation";
import Header from './Header';
import Sidebar from './Sidebar';
import ImageHoc from './Hoc/ImageHoc';
import Breadcrumbs from './Breadcrumbs';
import sidebar_ads from '../pages/ads/sidebar-ads.json';
import StickyBox from 'react-sticky-box';
import Footer from './Footer';
// type Props = {
//   children: React.ReactNode;
// };
export default function Layout({ children }) {
  const emptyAd = (data) => {
    for (let i in data) return false;
    return true;
  };
  return (
    <div className="root">
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="manifest" href="/site.webmanifest" />
        <link rel="apple-touch-icon" href="/icon.png" />
        <meta name="theme-color" content="#fff" />
      </Head>
      {/* <nav className="navbar container">
        <Navigation />
      </nav> */}
      <Header />
      <div className="container main-aside">
        <main className="main">{children}</main>
        {!emptyAd(sidebar_ads) && (<aside className="sidebar">
          <StickyBox offsetTop={80} offsetBottom={1}>
            <Sidebar  it={sidebar_ads}/>
          </StickyBox>
        </aside>)}
      </div>
      <Footer />
      <style jsx>
        {`
          .root {
            display: block;
            box-sizing: border-box;
            height: 100%;
          }
          @media (min-width: 769px) {
            // .root {
            //   display: flex;
            //   flex: 1 0 auto;
            // }
            // main {
            //   flex: 1 0 auto;
            // }
          }
        `}
      </style>
    </div>
  );
}
