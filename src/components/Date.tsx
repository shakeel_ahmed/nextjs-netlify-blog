import { format, formatISO } from "date-fns";

// type Props = {
//   date: Date;
// };
export default function Date({ date }) {
  return (
    <time dateTime={formatISO(date)}>
      <span>{format(date, "LLLL d, yyyy")}</span>
      <style jsx>
        {`
          span {
            margin-left: 4px;
          }
        `}
      </style>
    </time>
  );
}
