import Link from "next/link";
//import { TagContent } from "../lib/tags";

// type Props = {
//   tag: TagContent;
// };
export default function SubCategoryButton({ tag }) {
  return (
    <>
      <Link href={"/tags/[[...slug]]"} as={`/tags/${tag.slug}`}>
        <a>{tag.name}</a>
      </Link>
      <style jsx>{`
        a{
          color: #fff;
          background-color: #222222;
          padding: 3px 6px 4px 6px;
          white-space: nowrap;
          display: inline-block;
        }
        a:hover{
          opacity: 0.9;
        }
      `}</style>
    </>
  );
}
