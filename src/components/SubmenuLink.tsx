import SubmenuLinkItem from './SubmenuLinkItem';

const SubmenuLink = ({ it, inHover}) => {

    // const items = it.item_name;
    return (
        <div className="submenu-container" style={{display: inHover ? "block" : "none" }}>
            <div className="submenu-items-container">
                <SubmenuLinkItem />
                <SubmenuLinkItem />
                <SubmenuLinkItem />
                <SubmenuLinkItem />
                <SubmenuLinkItem />
            </div>
        </div>
    );
  }
  
export default SubmenuLink;