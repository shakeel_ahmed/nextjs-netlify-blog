import comments from '../pages/comments/pending-comments.json';
import Comment from '../components/Comment';

export default function Comments({ slug }){
    let result = [];
    comments.map((com) => {if(slug === com.slug) result.push(com)});
    console.log(result);
    return (
        <>
            <h1>Comments</h1>
            {
                result.map((it) => <Comment key={it._id} it={it}  /> )
            }
        </>
    )
}