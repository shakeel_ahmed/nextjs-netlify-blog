import Link from 'next/link';
import ImageHoc from './Hoc/ImageHoc';

const Sidebar = ({ it }) => {
    return (
        <>
        <div className="sidebar-image-wrap">
            {it.sidebar_ads && it.sidebar_ads.map((item, i) => {
                let src = `..${item.ad_image}`;
                return (<Link href={item.onclick_url} passHref={true} prefetch={false} key={i}>
                    <a target={"_blank"} title={item.ad_title}>
                        <ImageHoc alt={item.ad_title} src={src} />
                    </a>
                </Link>);
            } )}
        </div>
        <div className="sidebar-links">
            {it.heading && <h4>{it.heading}</h4>}
            <ul>
            {it.sidebar_links && it.sidebar_links.map((item, i) => {
                return (
                    <li key={i}>
                        <Link href={item.sidebar_link_url} passHref={true} prefetch={false}>
                            <a target={"_blank"} title={item.link_title}>
                                {item.link_title}
                            </a>
                        </Link>
                    </li>
                );
            })}
            </ul>
        </div>
        </>
    );

}

export default Sidebar;