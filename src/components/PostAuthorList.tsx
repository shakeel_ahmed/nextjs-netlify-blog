import React from "react";
//import { PostContent } from "../lib/posts";
import PostItem from "./PostItem";

export default function PostAuthorList({ posts }) {
  console.log("authorlist", posts);
  return (
    <>
    <div className={"posts-container"}>
      <div className={"posts"}>
        <ul className={"post-list"}>
        {posts.map((it, i) => (
            <li key={i}>
              <PostItem post={it} />
            </li>
          ))}
          {/* {posts.map((it, i) => (
            <li key={i}>
              <PostItem post={it} />
            </li>
          ))} */}
        </ul>
      </div>
      <style jsx>{`
        .posts-container {
          display: flex;
          width: 100%;
          flex-direction: column;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        li {
          list-style: none;
        }
        .posts {
          display: flex;
          flex-direction: column;
          flex: 1 1 auto;
        }
        .post-list {
          flex: 1 0 auto;
        }

        @media (min-width: 769px) {
          .tags {
            display: block;
          }
        }
        @media (max-width: 768px) {
          .container{
            flex-direction: column;
          }
        }
      `}</style>
    </div>
  </>
  );
}
