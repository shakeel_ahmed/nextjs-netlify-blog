import Link from "next/link";
import { useRouter } from "next/router";
import Burger from "./Burger";
import { useState } from "react";
import menu_items from "../pages/menus/main_menu.json";
import MenuLink from "../components/MenuLink";

export default function Navigation() {
  const router = useRouter();
  const [active, setActive] = useState(false);
  console.log(menu_items.menu_items);
  return (
    <>
      <Burger active={active} onClick={() => setActive(!active)} />
      <div className={active ? "active" : ""}>
        <ul>
          <li>
            <Link href="/">
              <a className={router.pathname === "/" ? "active" : null}><strong>Medlife</strong></a>
            </Link>
          </li>
          {menu_items.menu_items.map((it, i) =>(
            <li key={i}>
              <MenuLink it={it} index={i} />
            </li>
          ) ) }
          {/* <li>
            <Link href="/posts">
              <a
                className={
                  router.pathname.startsWith("/posts") ? "active" : null
                }
              >
                blog
              </a>
            </Link>
          </li> */}
        </ul>
        <style jsx>
          {`
            // .container {
            //   width: 0;
            // }
            ul {
              opacity: 0;
              width: 100%;
              list-style: none;
              margin: 0;
              padding: 0;
              position: fixed;
              top: 0;
              left: 0;
              background-color: #fff;
              transform: translateY(100%);
              transition: opacity 200ms;
            }
            .active ul {
              opacity: 1;
              transform: translateY(0);
            }
            li {
              margin-bottom: 1.75rem;
              font-size: 1.5rem;
              padding: 0 1.5rem 0 0;
            }
            li:last-child {
              margin-bottom: 0;
            }
            .active {
              color: #222;
            }

            @media (min-width: 769px) {
              // .container {
              //   width: 100%;
              //   display: block;
              // }
              ul {
                opacity: 1;
                width: 100%;
                display: block;
                transform: translateY(0);
              }
              li {
                font-size: 1rem;
                padding: 0;
              }
            }
          `}
        </style>
      </div>
    </>
  );
}
