import Link from 'next/link';
import menu_items from "../pages/menus/main_menu.json";
import social from '../../meta/social.json';
import Facebook from '../../public/images/facebook.svg';
import Instagram from '../../public/images/instagram.svg';
import Linkedin from '../../public/images/linkedin.svg';
import Twitter from '../../public/images/twitter.svg';
import Youtube from '../../public/images/youtube.svg';

const Footer = () => {
    const emptyAd = (data) => {
        for (let i in data) return false;
        return true;
    };

    return (
        <div className="footer">
            <div className="social">
                <div className="container">
                    <div className="row">
                        { !emptyAd(social) && (
                            <>
                                <span className="social-icon-wrap">
                                    <Link href={social.facebook} passHref={true} prefetch={false}>
                                        <a target="_blank">
                                            <i className="icon"><Facebook /></i>
                                            <span className="social-item">Facebook</span>
                                        </a>
                                    </Link>
                                </span>
                                <span className="social-icon-wrap">
                                    <Link href={social.instagram} passHref={true} prefetch={false}>
                                        <a target="_blank">
                                        <i className="icon"><Instagram /></i>
                                            <span className="social-item">Instagram</span>
                                        </a>
                                    </Link>
                                </span>
                                <span className="social-icon-wrap">
                                    <Link href={social.linkedin} passHref={true} prefetch={false}>
                                        <a target="_blank">
                                        <i className="icon"><Linkedin /></i>
                                            <span className="social-item">Linkedin</span>
                                        </a>
                                    </Link>
                                </span>
                                <span className="social-icon-wrap">
                                    <Link href={social.twitter} passHref={true} prefetch={false}>
                                        <a target="_blank">
                                            <i className="icon"><Twitter /></i>
                                            <span className="social-item">Twitter</span>
                                        </a>
                                    </Link>
                                </span>
                                <span className="social-icon-wrap">
                                    <Link href={social.youtube} passHref={true} prefetch={false}>
                                        <a target="_blank">
                                        <i className="icon"><Youtube /></i>
                                            <span className="social-item">Youtube</span>
                                        </a>
                                    </Link>
                                </span>
                            </>
                        )}
                    </div>
                </div>
            </div>
            <div className="copyright-wrap">
                <div className="container">
                    <div className="menu">
                        <ul className="menu-list">
                            { !emptyAd(menu_items) && (menu_items.menu_items.map((it, i) => {
                                    const item = it && it.custom_url ? ( 
                                        <li key={i}>
                                          <Link href={it.custom_url} passHref={true} prefetch={false} >
                                            <a target={"_blank"} >{it.title}</a>
                                          </Link>
                                        </li>
                                      ) : ( <li key={i}>
                                              <Link href={"/categories/[[...slug]]"} as={`/categories/${it.item_name}`} >
                                                <a>{it.title}</a>
                                              </Link>
                                            </li>
                                          ) 
                                    return item;
                                }))
                            }
                        </ul>
                    </div>
                    <div className="copyright">© Copyright 2020 - The content on website is proprietary information of Medlife Wellness Pvt. Ltd.</div>
                </div>
            </div>
        
        </div>
    );
}

export default Footer;