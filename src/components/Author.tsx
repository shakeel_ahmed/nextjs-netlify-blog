import { AuthorContent } from "../lib/authors";

// type Props = {
//   author: AuthorContent;
// };
export default function Author({ author }) {
  return (
    <>
      <span>{author.name}</span>
      <style jsx>
        {`
          span {
            font-weight: 700;
            margin-right: 3px;
            margin-left: 2px;
          }
        `}
      </style>
    </>
  );
}
