import Share from '../../public/images/share.svg';
import Facebook from '../../public/images/facebook.svg';

const SocialShareList = () => {
    return (
        <>
        <span className={"icon"}>
            <i>
            <Facebook width={14} height={14} fill={"#516eab"} />
            </i>
        </span>
        <span className={"text"}>
            Facebook
        </span>
        <style jsx>
            {`
            .icon, .text{
                line-height: 38px;
                border: 1px solid #e9e9e9;
            }
            .icon{
                border-right-width: 0;
                border-top-left-radius: 2px;
                border-bottom-left-radius: 2px;
                padding-left: 17px;
                padding-right: 17px;
                width: 40px;
                padding-left: 13px;
                padding-right: 13px;
                z-index: 1;
                display: inline-block;
                position: relative;
            }
            .icon i{
                top: -1px;
                left: -1px;
                vertical-align: middle;
                padding-left: 0;
            }
            .text{
                border-left-width: 0;
                border-top-right-radius: 2px;
                border-bottom-right-radius: 2px;
                font-weight: 700;
                margin-left: -6px;
                padding-left: 12px;
                padding-right: 17px;
                display: inline-block;
                position: relative;
                color: #516eab;
            }
            .text:before{
                background-color: #000;
                opacity: 0.08;
                content: '';
                position: absolute;
                top: 12px;
                left: 0;
                width: 1px;
                height: 16px;
                z-index: 1;
            }
            .icon i svg{
                width: 14px !important;
                height: 14px !important;
                fill; #516eab;
            }
            `}
        </style>
        </>
    );
}

export default SocialShareList;