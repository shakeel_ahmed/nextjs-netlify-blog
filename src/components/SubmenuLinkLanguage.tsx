import Link from 'next/link';

const SubmenuLinkLanguage = () => {
    return (
        <div className="submenu-lang-container">
            <div className="submenu-lang-items">
                <Link href="/" as="/">
                    <a title="English">EN</a>
                </Link>
            </div>
        </div>
    );
}

export default SubmenuLinkLanguage;