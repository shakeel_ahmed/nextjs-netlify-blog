import Link from 'next/link';
import ImageHoc from '../components/Hoc/ImageHoc';

const SubmenuLinkItem = () => {
    // const items = it.item_name;
    return (
        <div className="submenu-items">
            <div className="p-relative">
                <div className="image">
                    <Link as="/" href="/" >
                        <a rel="bookmark" title="Are You serious">
                            <ImageHoc alt="wellness tips posts" src="../images/thumb-1.jpg" />
                        </a>
                    </Link>
                </div>
                <Link href="/" as="/">
                    <a className="p-absolute submenu-tag">
                        sleepapnea
                    </a>
                </Link>
            </div>
            <div className="submenu-item-title">
                <h3>
                    <Link href="/" as="/">
                        <a>Are You Yawning Excessively?</a>
                    </Link>
                </h3>
            </div>
        </div>
    );
  }
  
export default SubmenuLinkItem;