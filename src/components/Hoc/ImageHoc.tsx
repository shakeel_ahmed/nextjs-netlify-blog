import React from 'react';
import { LazyLoadImage } from 'react-lazy-load-image-component';

const ImageHoc = ({ alt, src, ...rest }) => (
    <div>
        <LazyLoadImage
            alt={alt}
            src={src}
            {...rest}
        />

    </div>
);

export default ImageHoc;
