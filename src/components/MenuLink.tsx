import Link from "next/link";
import { useState } from 'react';
import SubmenuLink from './SubmenuLink';
import DownArrow from '../../public/images/down-arrow.svg';

const MenuLink = ({ it, index }) => {
  // const url = it.custom_url.replace(/^https?:\/\//,'');
  const [inHover, setHover] = useState(false);
  const item = it && it.custom_url ? ( 
    <>
      <Link href={it.custom_url} passHref={true} prefetch={false} >
        <a target={"_blank"} >{it.title}</a>
      </Link>
    </>
  ) : ( <>
          <Link href={"/categories/[[...slug]]"} as={`/categories/${it.item_name}`} >
            <a onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)}>{it.title}<i><DownArrow /></i></a>
          </Link>
          <SubmenuLink it={it} inHover={inHover} />
        </>
      ) 
  return item;
}

export default MenuLink;
