export default function Comment({ it }){
    return (
        <>
            <h3>{it.username}</h3>
            <small>{it.date}</small>
            <p>{it.comment}</p>
        </>
    )
}