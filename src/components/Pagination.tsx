import { generatePagination } from "../lib/pagination";
import Link from "next/link";

// type Props = {
//   current: number;
//   pages: number;
//   link: {
//     href: (page: number) => string;
//     as: (page: number) => string;
//   };
// };
export default function Pagination({ current, pages, link }) {
  const pagination = generatePagination(current, pages);
  return (
    <div className="pagination">
      <ul>
      {pagination.map((it, i) => (
        <li key={i} className={it.page === current ? "active" : null}>
          {it.excerpt ? (
            <a onClick={() => false}>...</a>
          ) : (
            <Link href={link.href(it.page)} as={link.as(it.page)}>
              <a>{it.page}</a>
            </Link>
          )}
        </li>
      ))}
    </ul>
    <span className="pages">Page {current} of {pages}</span>
      <style jsx>{`
        .pagination{
          margin: 30px 0 30px 0;
          font-size: 13px;
          display: inline-block;
          width: 100%;
        }
        .pagination ul li{
          padding: 1px 1px;
          border: 1px solid #e3e3e3;
          display: inline-block;
          margin: 0 8px 8px 0;
          min-width: 33px;
          text-align: center;
          color: #666;
          line-height: 21px;
          float: left;
        }
        .pagination ul li a{
          display: block;
          padding: 4px 10px;
          color: #666;
        }
        .pagination ul li:hover{
          color: #fff;
          background-color: #444;
          border-color: #444;
        }
        .pagination ul li.active{
          border-color: #ff7300;
          background-color: #ff7300;
          color: #fff !important;
        }
        .pagination ul li.active a{
          color: #fff !important;
        }
        a:hover{
          color: #fff !important;
          font-weight: bold;
        }
        .pages{
          float: right;
          margin: 0;
          border: none;
          padding: 6px 0 6px 6px;
        }
      `}</style>
    </div>
  );
}
