// import { PostContent } from "../lib/posts";
import Date from "./Date";
import Link from "next/link";
import { parseISO } from "date-fns";
import ImageHoc from './Hoc/ImageHoc';

// type Props = {
//   post: PostContent;
// };
export default function PostItem({ post }) {
  const src = `..${post.feature_image}`;
  const excerpt = post.excerpt ? post.excerpt.substring(0, 195) : " ";
  
  return (
    <div className="post-item">
      <div className="post-item-thumb">
        <Link href={"/posts/" + post.slug} >
          <a title={post.title}>
            <ImageHoc alt={post.title} src={src} />
          </a>
        </Link>
      </div>
      <div className="post-item-detail">
        <h3>
          <Link href={"/posts/" + post.slug}>
            <a>
              {post.title}
            </a>
          </Link>
        </h3>
        <div className="post-item-meta">
          <span className="author">{post.author}<span className="space">-</span></span>
          <span className="date"><Date date={parseISO(post.date)} /></span>
          <span className="comments">
            <Link href="/">
              <a title="Comments">0</a>  
            </Link>
          </span>
        </div>
        <div className="excerpt">
          {excerpt}...
        </div>
        <div className="read-more">
          <Link href={"/posts/" + post.slug}>
            <a>
              Read more
            </a>
          </Link>
        </div>
      </div>

        <style jsx>
          {`
            .post-item{
              padding-bottom: 48px;
              position: relative;
            }
            .post-item-thumb{
              position: absolute;
              left: 0;
              top: 0;
            }
            .post-item-thumb img{
              width: 324px;
              height: auto;
            }
            .post-item-detail {
              margin-left: 350px;
              min-height: 235px;
            }
            .post-item-detail h3{
              font-size: 25px;
              font-weight: bold;
              line-height: 29px;
              margin-bottom: 11px;
              color: #111111;
              margin: 0 0 10px 0;
            }
            .post-item-detail h3 a:hover{
              color: #ff7300;
            }
            .post-item-meta{
              font-size: 11px;
              margin-bottom: 7px;
              line-height: 1;
              min-height: 17px;
              color: #aaa;
            }
            .post-item-meta .author{
              font-weight: bold;
              display: inline-block;
              position: relative;
              top: 2px;
              color: #000;
              letter-spacing: 0.5px;
            }
            .post-item-meta .space {
              color: #ccc;
              margin: 0 4px 0 4px;
              font-weight: normal;
            }
            .post-item-meta .date{
              color: #aaa;
              display: inline-block;
              position: relative;
              top: 2px;
            }
            .post-item-meta .comments{
              position: relative;
              float: right;
              font-size: 10px;
              font-weight: 600;
              text-align: center;
              line-height: 1;
            }
            .post-item-meta .comments a{
              color: #fff;
              background-color: #000;
              display: inline-block;
              min-width: 17px;
              padding: 3px 4px 4px 5px;
              position: relative;
            }
            .post-item-meta .comments a:after{
              position: absolute;
              bottom: -3px;
              left: 0;
              content: '';
              width: 0;
              height: 0;
              border-style: solid;
              border-width: 3px 3px 0 0;
              border-color: #000 transparent transparent transparent;
            }
            .post-item-detail .excerpt{
              font-size: 13.5px;
              color: #777;
              margin-top: 9px;
              margin-bottom: 15px;
            }
            .post-item-detail .read-more a{
              background-color: #ff7300;
              color: #fff;
              display: inline-block;
              font-size: 13px;
              font-weight: 500;
              line-height: 1;
              padding: 10px 15px;
              -webkit-transition: background-color 0.4s;
              transition: background-color 0.4s;
            }
            .post-item-detail .read-more a:hover{
              background-color: #000;
              color: #fff;
            }
            a {
              color: #222;
              display: inline-block;
            }
            h2 {
              margin: 0;
              font-weight: 500;
            }
          `}
        </style>
    </div>
  );
}
