import Link from "next/link";
import { useRouter } from "next/router";
import Burger from "./Burger";
import { useState } from "react";
import menu_items from "../pages/menus/main_menu.json";
import MenuLink from "../components/MenuLink";
import SubmenuLink from "../components/SubmenuLink";
import ImageHoc from './Hoc/ImageHoc';
import MenuLinkLanguage from './MenuLinkLanguage';
import SubmenuLinkLanguage from './SubmenuLinkLanguage';
import HeaderAds from './HeaderAds';
import header_ad from '../pages/ads/header-ads.json';

export default function Header() {
  const router = useRouter();
  const [active, setActive] = useState(false);
  const emptyAd = (data) => {
      for (let i in data) return false;
      return true;
  };

  return (
    <>
      {/* <Burger active={active} onClick={() => setActive(!active)} /> */}
      <div className="header-main">
          <div className="header-wrap">
            <nav className="gradient-menu">
                <div className="container nav-container">
                    <div className="menu">
                        <div className="logo">
                            <Link as="/" href="/">
                                <a>
                                    <ImageHoc alt="Medlife-logo" src="../images/medlife-logo.png" className="desktop-medlifeLogo" />
                                </a>
                            </Link>
                        </div>
                        <div className="menu-items-container">
                            <ul className="menu-items-list">
                                {menu_items.menu_items.map((it, i) =>(
                                    <li key={i} className="menu-items-individual" >
                                        <MenuLink it={it} index={i}/>
                                    </li>
                                ))}
                                <li className="menu-items-individual language">
                                    <MenuLinkLanguage />
                                    <SubmenuLinkLanguage />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
          </div>
          {!emptyAd(header_ad) && <HeaderAds title={header_ad.ad_title} url={header_ad.onclick_url} image={header_ad.ad_image} />}
      </div>
      {/* <div className={active ? "active" : ""}></div> */}

    </>
  );
}
