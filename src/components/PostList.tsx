import React from "react";
//import { PostContent } from "../lib/posts";
import PostItem from "./PostItem";
import TagLink from "./TagLink";
import CategoryLink from "./CategoryLink";
import Pagination from "./Pagination";
import Breadcrumbs from './Breadcrumbs';

export default function PostList({ posts, tags, categories, pagination }) {
  return (
    <>
    <Breadcrumbs />
    <div className={"posts-container"}>
      <div className={"posts"}>
        <ul className={"post-list"}>
          {posts.map((it, i) => (
            <li key={i}>
              <PostItem post={it} />
            </li>
          ))}
        </ul>
        <Pagination
          current={pagination.current}
          pages={pagination.pages}
          link={{
            href: (page) => (page === 1 ? "/" : "/page/[page]"),
            as: (page) => (page === 1 ? null : "/page/" + page),
          }}
        />
      </div>
      {/* <ul className={"tags"}>
        <strong>Tags</strong>
        <br />
        <br />
        {tags.map((it, i) => (
          <li key={i}>
            <TagLink tag={it} />
          </li>
        ))}
      </ul>
      <br />
      <br />
      <ul className={"tags"}>
        <strong>Categories</strong>
        <br />
        <br />
        {categories.map((it, i) => (
          <li key={i}>
            <CategoryLink category={it} />
          </li>
        ))}
      </ul> */}
      <style jsx>{`
        .posts-container {
          display: flex;
          width: 100%;
          flex-direction: column;
        }
        ul {
          margin: 0;
          padding: 0;
        }
        li {
          list-style: none;
        }
        .posts {
          display: flex;
          flex-direction: column;
          flex: 1 1 auto;
        }
        .post-list {
          flex: 1 0 auto;
        }
        .tags li {
          margin-bottom: 0.75em;
        }

        @media (min-width: 769px) {
          .tags {
            display: block;
          }
        }
        @media (max-width: 768px) {
          .container{
            flex-direction: column;
          }
        }
      `}</style>
    </div>
  </>
  );
}
