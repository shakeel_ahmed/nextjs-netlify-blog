import Link from 'next/link';
import DownArrow from '../../public/images/down-arrow.svg';

const MenuLinkLanguage = () => <Link href="/" as="/"><a>EN<i><DownArrow /></i></a></Link>
export default MenuLinkLanguage;