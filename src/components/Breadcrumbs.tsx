import Link from 'next/link';

const Breadcrumbs = () => {
    return (
        <div className='container breadcrumbs'>
            <Link href="/">
                <a>
                    Home
                </a>
            </Link>
        </div>
    );
}

export default Breadcrumbs;