import Link from 'next/link';
import Share from '../../public/images/share.svg';
import SocialShareList from './SocialShareList';

const SharePost = () => {
    return (
        <div className={"share"}>
            <div className={"share-wrap"}>
                <div className={"share-main-button"}>
                    <div className={"icon"}>
                        <i><Share /></i>
                    </div>
                    <div className={"text"}>
                        Share
                    </div>
                </div>
                <Link href="/">
                    <a>
                        <SocialShareList />
                    </a>
                </Link>
            </div>
            <style jsx>
                {`
                .share{
                    border-style: solid;
                    border-color: #ededed;
                    border-width: 1px 0;
                    padding: 21px 0;
                    margin-bottom: 42px;
                }
                .share-wrap{
                    margin-bottom: -7px;
                    transition: opacity 0.3s;
                    opacity: 1;
                    white-space: normal;
                    display: inline-block;
                }
                .share-main-button{
                    transition: opacity 0.2s ease 0s;
                    opacity: 1;
                    color: #444;
                    border: 1px solid #e9e9e9;
                    border-radius: 2px;
                    position: relative;
                    display: inline-block;
                    margin: 0 3px 7px;
                    height: 40px;
                    min-width: 40px;
                    font-size: 11px;
                    text-align: center;
                    margin-right: 18px;
                    vertical-align: middle;
                }
                .share-main-button:before{
                    border-width: 9px 0 9px 11px;
                    border-color: transparent transparent transparent #e9e9e9;
                    content: '';
                    position: absolute;
                    top: 50%;
                    -webkit-transform: translateY(-50%);
                    left: 100%;
                    width: 0;
                    height: 0;
                    border-style: solid;
                }
                .share-main-button .icon{
                    width: 40px;
                    border-color: #e9e9e9;
                    padding-left: 13px;
                    padding-right: 13px;
                    line-height: 40px;
                    z-index: 1;
                    display: inline-block;
                    position: relative;
                }
                .share-main-button .icon i{
                    top: -1px;
                    left: -1px;
                    vertical-align: middle;
                    padding-left: 0;
                }
                .share-main-button .text{
                    border-color: #e9e9e9;
                    font-weight: 700;
                    margin-left: -6px;
                    padding-left: 12px;
                    padding-right: 17px;
                    line-height: 40px;
                    display: inline-block;
                    position: relative;
                }
                .share-main-button .text:before{
                    background-color: #000;
                    opacity: 0.08;
                    content: '';
                    position: absolute;
                    top: 12px;
                    left: 0;
                    width: 1px;
                    height: 16px;
                    z-index: 1;
                }
                .share-wrap a{
                    transition: opacity 0.2s ease 0s;
                    opacity: 1
                    color: #000;
                    overflow: hidden;
                    position: relative;
                    display: inline-block;
                    margin: 0 3px 7px;
                    height: 40px;
                    min-width: 40px;
                    font-size: 11px;
                    text-align: center;
                    vertical-align: middle;
                }
                .share-wrap a:hover{
                    opacity: 0.8 !important;
                }
                `}
            </style>
        </div>
    );
}

export default SharePost;