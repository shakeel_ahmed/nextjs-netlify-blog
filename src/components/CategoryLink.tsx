import Link from "next/link";

export default function Category({ category }) {
  return (
    <Link href={"/categories/[[...slug]]"} as={`/categories/${category.slug}`}>
      <a>{"#" + category.name}</a>
    </Link>
  );
}
