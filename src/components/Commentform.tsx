
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useMutate } from 'restful-react';
import { useRouter } from 'next/router';


export default function Comment({ slug }){
  const [info, setInfo] = useState('');
  const { mutate: registerUser, loading, error } = useMutate({
    verb: 'POST',
    path: 'register'
  });
  const { register, handleSubmit } = useForm();

  const onSubmit = data => {
    data = {...data, slug};
    setInfo('');
    registerUser(data)
       .then(res => setInfo(res.message));
  }
    return (
        <>
            {info === '' ?  (
                <form onSubmit={handleSubmit(onSubmit)} className="comment-form" >
                <div className="clearfix"></div>
                <div className="comment-form-input-wrap td-form-comment">
                    <input ref={register} name="username" placeholder="Username" type="text" aria-required="true" />
                </div>
                <div className="comment-form-input-wrap td-form-author">
                    <input ref={register} name="email" placeholder="Email:*" type="email" aria-required="true" />
                </div>
                <div className="comment-form-input-wrap td-form-email">
                    <textarea ref={register} name="comment" placeholder="Comment:*" aria-required="true"></textarea>
                </div>
                <p className="form-submit">
                    <input name="submit" type="submit" id="submit" className="submit" value="Post Comment" />
                </p>
            </form>
            ) : info }
        </>
    )
}

