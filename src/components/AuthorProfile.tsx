import Link from 'next/link';
import ImageHoc from './Hoc/ImageHoc';

const AuthorProfile = ({ author }) => {
    const image_url = `..${author.image}`;
    return (
        <div className={"author-wrap"}>
            <Link href="/">
                <a>
                    <ImageHoc src={image_url} alt={author.name} className="author-image"/>
                </a>
            </Link>
            <div className={"desc"}>
                <div className={"author-name vcard author"}>
                    <span className={"fn"}>
                        <Link href="/">
                            <a>{author.name}</a>
                        </Link>
                    </span>
                </div>
                <div className={"about"}>
                    {author.about}
                </div>
            </div>
            <style jsx>
                {`
                .author-wrap{
                    border: 1px solid #ededed;
                    padding: 21px;
                    margin-bottom: 48px;
                    font-family: "Open Sans", sans-serif;
                }
                .desc{
                    margin-left: 117px;
                    font-size: 12px;
                }
                .author-name{
                    font-size: 15px;
                    line-height: 21px;
                    font-weight: 700;
                    margin: 7px 0 8px 0;
                }
                .author-name a{
                    color: #222;
                }
                .author-name a:hover{
                    color: #ff7300;
                }
                `}
            </style>
        </div>
    );
}
export default AuthorProfile;