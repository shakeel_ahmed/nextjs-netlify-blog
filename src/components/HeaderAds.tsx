import Link from 'next/link';
import ImageHoc from './Hoc/ImageHoc';

const HeaderAds = ({ title, url , image }) => {
    const image_url = `..${image}`;
    return (
        // <div className="header-ads">
        //       <div className="header-ads-wrap">
        //       </div>
        //   </div>
        <Link href={url} passHref={true} prefetch={false}>
            <a target={"_blank"} title={title}>
                <ImageHoc alt={title} src={image_url} className="header-ad-img"/> 
            </a>
        </Link>
    );
}
export default HeaderAds;