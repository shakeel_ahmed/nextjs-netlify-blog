import categories from "../../meta/categories.yml";

const categoryMap = generateCategoryMap();

function generateCategoryMap(){
  let result = {};
  for (const category of categories.categories) {
    result[category.slug] = category;    
  }
  return result;
}

export function getCategory(slug) {
  return categoryMap[slug];
}

export function listCategories(){
  return categories.categories;
}
