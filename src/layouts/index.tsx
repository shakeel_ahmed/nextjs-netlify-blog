const fs = require("fs");
const path = require("path");
const yaml = require("js-yaml");
const matter = require("gray-matter");
// import fs from "fs";
// import path from 'path';
// import yaml from "js-yaml";
// import matter from "gray-matter";
import Head from "next/head";
import React from "react";
import Link from 'next/link';
import styles from "../../public/styles/content.module.css";
import Author from "../components/Author";
import Copyright from "../components/Copyright";
import Date from "../components/Date";
import Layout from "../components/Layout";
import BasicMeta from "../components/meta/BasicMeta";
import JsonLdMeta from "../components/meta/JsonLdMeta";
import OpenGraphMeta from "../components/meta/OpenGraphMeta";
import TwitterCardMeta from "../components/meta/TwitterCardMeta";
import { SocialList } from "../components/SocialList";
import TagButton from "../components/TagButton";
import { getAuthor } from "../lib/authors";
import { getTag } from "../lib/tags";
// import Commentform from '../components/Commentform'
import Comments from '../components/Comments';
import Breadcrumbs from '../components/Breadcrumbs';
import SubCategoryButton from '../components/SubCategoryButton';
import Chat from '../../public/images/chat.svg';
import SharePost from '../components/SharePost';
import AuthorProfile from '../components/AuthorProfile';
import { listPostContent } from '../lib/posts';
import PostAuthorList from '../components/PostAuthorList';

// export type PostContent = {
//   readonly date: string;
//   readonly title: string;
//   readonly slug: string;
//   readonly tags?: string[];
// };

// type Props = {
//   title: string;
//   date: Date;
//   slug: string;
//   description: string;
//   tags: string[];
//   author: string;
//   posts: PostContent[];
// };
export default function Index({
  title,
  date,
  slug,
  author,
  tags,
  description,
  posts
}) {
  const keywords = tags.map((it) => getTag(it).name);
  const authorName = getAuthor(author).name;
  
  const post =  listPostContent(1,9);
  console.log("layout",posts);
  return ({ children: content }) => {
    return (
      <Layout>
        <BasicMeta url={`/posts/${slug}`} title={title} keywords={keywords} description={description} />
        <TwitterCardMeta url={`/posts/${slug}`} title={title} description={description} />
        <OpenGraphMeta url={`/posts/${slug}`} title={title} description={description} />
        <JsonLdMeta url={`/posts/${slug}`} title={title} keywords={keywords} date={date} author={authorName} description={description} />
        <Breadcrumbs />
        <div className={"post"}>
          <div className="clearfix"></div>
          <article id={slug}>
            <div className={"post-header"}>
              <ul className={"subcategory-list"}>
                {tags.map((it, i) => (
                  <li className={"subcategory-list-item"} key={i}>
                    <SubCategoryButton tag={getTag(it)} />
                  </li>
                ))}
              </ul>
              <header>
                <h1>{title}</h1>
                <div className={"meta-info"}>
                  <span>By <Author author={getAuthor(author)} /> - <Date date={date} /></span>
                  <span className={"comments"}>
                    <Link href="#comment">
                      <a><i><Chat /></i>0</a>
                    </Link>
                  </span>
                </div>
              </header>
            </div>
            <div className="post-content">
              {content}
            </div>
            <footer>
              <div className={"tags"}>
                <ul>
                  <li><span>TAGS</span></li>
                  <li>
                    <Link href="/" as="/">
                      <a>Baby Care</a>
                    </Link>
                  </li>
                </ul>
              </div>
              <SharePost />
              <AuthorProfile author={getAuthor(author)} />
            </footer>
          </article>
        {/* <PostAuthorList posts={fetchPostContents()} />   */}
        </div>
        
        <div>
          <article>
            <header>
            </header>
            {/* <div className={styles.content}>{content}</div> */}
          </article>
          <div id="comment">
          <Comments slug={slug} />
          </div>
          {/* <Commentform slug={slug} /> */}
          <footer>
            <div className={"social-list"}>
              <SocialList />
            </div>
            <Copyright />
          </footer>
        </div>
        <style jsx>
          {`
            .post{
              overflow: auto;
            }
            .subcategory-list{
              font-family: 'Open Sans', arial, sans-serif;
              font-size: 10px;
              margin-top: 0;
              margin-bottom: 10px;
              line-height: 1;
            }
            .subcategory-list-item{
              display: inline-block;
              margin: 0 5px 5px 0;
              line-height: 1;
            }
            .post-header h1{
              font-size: 41px;
              line-height: 50px;
              margin: 0 0 0.5rem;
            }
            .meta-info{
              font-family: 'Open Sans', arial, sans-serif;
              font-size: 11px;
              margin-bottom: 16px;
              line-height: 1;
              min-height: 17px;
            }
            .comments{
              margin-left: 22px;
              float: right;
              position: relative;
            }
            .comments a{
              color: #444;
            }
            .comments i{
              margin-right:5px;
            }
            .post article footer{
              clear: both;
            }
            footer .tags{
              font-size: 11px;
            }
            footer .tags ul{
              margin: 0 0 30px 0;
              font-family: 'Open Sans', arial, sans-serif;
              display: table;
              line-height: 20px;
              font-weight: 600;
              clear: left;
              height: 20px;
            }
            footer .tags ul li{
              display: inline-block;
              line-height: 20px;
              margin-left: 0;
              float: left;
              margin-bottom: 4px;
            }
            footer .tags ul li span{
              background-color: #222222;
              padding: 5px 9px;
              color: #fff;
              display: block;
              text-transform: uppercase;
              line-height: 10px;
              float: left;
              height: 20px;
            }
            footer .tags ul li span, footer .tags ul li a{
              margin-right: 4px;
              margin-left: 0;
            }
            footer .tags ul li a{
              display: block;
              float: left;
              border: 1px solid #ededed;
              margin-left: 4px;
              line-height: 8px;
              color: #111111;
              padding: 5px 8px;
              height: 20px;
              transition: color 0.1s ease;
            }
            footer .tags ul li a:hover{
              border-color: #ff7300;
              background-color: #ff7300;
              color: #fff;
            }
            .container {
              display: block;
              max-width: 36rem;
              width: 100%;
              margin: 0 auto;
              padding: 0 1.5rem;
              box-sizing: border-box;
            }
            .metadata div {
              display: inline-block;
              margin-right: 0.5rem;
            }
            article {
              flex: 1 0 auto;
            }
            .tag-list {
              list-style: none;
              text-align: right;
              margin: 1.75rem 0 0 0;
              padding: 0;
            }
            .tag-list li {
              display: inline-block;
              margin-left: 0.5rem;
            }
            .social-list {
              margin-top: 3rem;
              text-align: center;
            }

            @media (min-width: 769px) {
              .container {
                display: flex;
                flex-direction: column;
              }
            }
          `}
        </style>
        <style global jsx>
          {`
            /* Syntax highlighting */
            .token.comment,
            .token.prolog,
            .token.doctype,
            .token.cdata,
            .token.plain-text {
              color: #6a737d;
            }

            .token.atrule,
            .token.attr-value,
            .token.keyword,
            .token.operator {
              color: #d73a49;
            }

            .token.property,
            .token.tag,
            .token.boolean,
            .token.number,
            .token.constant,
            .token.symbol,
            .token.deleted {
              color: #22863a;
            }

            .token.selector,
            .token.attr-name,
            .token.string,
            .token.char,
            .token.builtin,
            .token.inserted {
              color: #032f62;
            }

            .token.function,
            .token.class-name {
              color: #6f42c1;
            }

            /* language-specific */

            /* JSX */
            .language-jsx .token.punctuation,
            .language-jsx .token.tag .token.punctuation,
            .language-jsx .token.tag .token.script,
            .language-jsx .token.plain-text {
              color: #24292e;
            }

            .language-jsx .token.tag .token.attr-name {
              color: #6f42c1;
            }

            .language-jsx .token.tag .token.class-name {
              color: #005cc5;
            }

            .language-jsx .token.tag .token.script-punctuation,
            .language-jsx .token.attr-value .token.punctuation:first-child {
              color: #d73a49;
            }

            .language-jsx .token.attr-value {
              color: #032f62;
            }

            .language-jsx span[class="comment"] {
              color: pink;
            }

            /* HTML */
            .language-html .token.tag .token.punctuation {
              color: #24292e;
            }

            .language-html .token.tag .token.attr-name {
              color: #6f42c1;
            }

            .language-html .token.tag .token.attr-value,
            .language-html
              .token.tag
              .token.attr-value
              .token.punctuation:not(:first-child) {
              color: #032f62;
            }

            /* CSS */
            .language-css .token.selector {
              color: #6f42c1;
            }

            .language-css .token.property {
              color: #005cc5;
            }
          `}
        </style>
      </Layout>
    );
  };
}

export const getStaticProps = async () => {
  const posts = listPostContent(1, 9);
  return {
    props: {
      posts,
    },
  };
};

// export async function getStaticProps() {
//   const postsDirectory = path.join(process.cwd(), 'src/pages/posts');
//   const filenames = await fs.readdir(postsDirectory);
  
//   const posts = filenames
//   .filter((it) => it.endsWith(".mdx"))
//   .map(async (filename) => {
//     const filePath = path.join(postsDirectory, filename)
//     const fileContents = await fs.readFile(filePath, 'utf8')

//     // Generally you would parse/transform the contents
//     // For example you can transform markdown to HTML here
//     const matterResult = matter(fileContents, {
//       engines: {
//         yaml: (s) => yaml.safeLoad(s, { schema: yaml.JSON_SCHEMA }) as object,
//       },
//     });
//     const matterData = matterResult.data;
//     const slug = filename.replace(/\.mdx$/, "");

//     if (matterData.slug !== slug) {
//       throw new Error(
//         "slug field not match with the path of its content source"
//       );
//     }
//     return matterData;
//   });

//   // By returning { props: posts }, the Blog component
//   // will receive `posts` as a prop at build time
//   return {
//     props: {
//       posts: await Promise.all(posts),
//     },
//   }
// }