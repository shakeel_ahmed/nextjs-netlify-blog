// import { NowRequest, NowResponse } from "@now/node";
import { create, renderBody } from "./_lib/oauth2";

export default async ( req, res ) => {
  const code = req.query.code;
  const { host } = req.headers;

  const oauth2 = create();

  try {
    const accessToken = await oauth2.authorizationCode.getToken({
      code,
      redirect_uri: `http://${host}/api/callback`
    });
    const { token } = oauth2.accessToken.create(accessToken);

    res.status(200).send(
      renderBody("success", {
        token: token.access_token,
        provider: "gitlab"
      })
    );
  } catch (e) {
    res.status(200).send(renderBody("error", e));
  }
};
